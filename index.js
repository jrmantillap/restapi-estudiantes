const express = require('express');
const app = express();

app.use(express.json());

const estudiantes = [
    {id: 1, nombre: 'Ricardo',apellido: 'Mantilla', edad: 24, inscripcion: false },
    {id: 2, nombre: 'Karen',apellido: 'Trujillo', edad: 26, inscripcion: false },
    {id: 3, nombre: 'Viviana',apellido: 'Pazminio', edad: 25, inscripcion: false },
    {id: 4, nombre: 'Fernanda',apellido: 'Pucachaqui', edad: 22, inscripcion: false },
    {id: 5, nombre: 'Micaela',apellido: 'Guasgua', edad: 21, inscripcion: false },
];


app.get('/', (req, res) => {
    res.send('Node Js api');
});


app.get('/api/estudiantes', (req, res) => {
    res.send(estudiantes);
});

app.get('/api/estudiantes/:id', (req, res) => {
    const estudiante = estudiantes.find(c => c.id === parseInt(req.params.id));
    if(!estudiante) return res.status(404).send('Estudiante no encontrado');
    else res.send(estudiante);
});

app.post('/api/estudiantes', (req,res) => {
    const estudiante = {
        id: estudiantes.length + 1,
        nombre: req.body.name,
        apellido: req.body.apellido,
        edad: parseInt(req.body.edad),
        inscripcion:  (req.body.inscripcion === 'true'),
    };

    estudiantes.push(estudiante);
    res.send(estudiante);

});


app.delete('/api/estudiantes/:id', (req, res) => {
    const estudiante = estudiantes.find(c => c.id === parseInt(req.params.id));
    if(!estudiante) return res.status(404).send('Estudiante no encontrado');
    
    const index = estudiantes.indexOf(estudiante);
    estudiantes.splice(index,1);
    res.send(estudiante);
});

const port = process.env.port || 80;
app.listen(port, () => console.log(`Escuchando en puerto ${port}...`));






